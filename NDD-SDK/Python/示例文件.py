# -*- coding: utf-8 -*-
# author:Tiper(邱鹏)
# 文件所属项目:NDD SDK
# 文件描述:NDD sdk的演示文件
import random
import QuanmNddSDK


open_id = '2'  # 开发者ID  请修改替换：open_id\lcode_cipher\PublicKeyStr
# lcode一般不会变动，推荐您用第三方工具将您的lcode使用公钥加密后粘贴进来
lcode_cipher = "EJnzqJsDTMt1aSaDbhp3zuecv4bia/J1yuna+rTItMinJ3e43Hs0pGR5uyvBaPEL+YeP8gtthzPhAfkoOKpIbbof8VeY4MdZow3HXmgwfNld4xq42QiPAfKCjlJ40H48jxc5Zqg6hIO0p+KfjXp1GST547z2QaieuLEYUS449Kc="  # lcode密文
PublicKeyStr = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqASIb3DQEBAQUAA4GNADCBiQKBgQC8Ql3WNz2XZyc6ESClqP5LigLk\nPVDUYSBIcremoTFsvBWg50C1uMJebRY1cNyK5o0hg7sdezJiL5D6kwY05x6Lx2Ms\nEX5DCOh/Kp0doSWecl5OdegxNn9M1EWzzjIEydWB9kew7kgYXhIrzhqRuQ1IpyNl\nEL91QP7MuPczwXtlAQIDAQAB\n-----END PUBLIC KEY-----"

ndd_sdk = QuanmNddSDK.SDK(open_id, lcode_cipher, PublicKeyStr)  # 实例化SDK
# 这里演示了简单的操作
random_data = random.randint(100000, 999999)  # 生成随机数据
# 您的数据
test_data = {
    "number":  59999
}

# 您可以逐行解除注释来查看调用效果(一次解除一行)
# info, data, tid = ndd_sdk.check_status()
# info, data, tid = ndd_sdk.get_data('PythonSDK_Test_003')  # 拉取数据，数据为空时返回标准的空数据
# info, data, tid = ndd_sdk.set_data('PythonSDK_Test_003', test_data)
# info, data, tid = ndd_sdk.get_data_many('number>1000')  # 拉取数据，数据为空时返回标准的空数据
# info, data, tid = ndd_sdk.del_data('PythonSDK_Test_003')
info, data, tid = ndd_sdk.restore_data('16')
print(info)
print(data)
print(tid)
