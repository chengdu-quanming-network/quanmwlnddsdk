# -*- coding: utf-8 -*-
# author:Tiper(邱鹏)
# 文件所属项目:QDC NDD SDK
# 文件描述:QuanmNDD SDK (泉鸣开放平台ndd接口SDK)，包含执行短信业务所需的方法
# Python版本要求：Python3及以上（可自行修改兼容Python2）
# 官网：dev.quanmwl.com
# 发布日期:2022-11-11
# 更新日期:2023-10-14（重写）
import base64
import json
import random
import hashlib
from copy import copy

import requests
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher_PKC

hl = hashlib.md5()
# 如果你从未成功使用过Crypto库，请卸载.
# 然后执行 pip install pycryptodome
# 如果您不知道这个库的问题，请参考https://blog.csdn.net/qq_41501331/article/details/118655589


class SDK:
    def __init__(self, openid, lcode_cipher, publickey, debug=False, en='rsa'):
        # type: (str, str, str, bool, str) -> SDK
        # 请开发者修改下列三行配置信息
        self.open_id = openid
        # lcode一般不会变动，推荐您用第三方工具将您的lcode使用公钥加密后粘贴进来（比如：https://oktools.net/rsa）
        self.lcode_cipher = lcode_cipher
        # 公钥, 将下载的公钥pem文件中内容复制，换行处使用“\n”代替（可参考如下配置）
        self.PublicKeyStr = publickey
        # 是否开启debug输出
        self.debug = debug
        # 加密方式
        self.en = en

        self.api_host = 'http://dev.quanmwl.com'  # Api Host【默认，api支持https，如有需要请修改】

        self.state_code = {
            "200": "请求成功",
            "201": "请求信息有误，请检查提交的信息",
            "202": "账户数据冲突，请联系本站(软件)开发者解决",
            "203": "服务器处理失败",
            "204": "请求的服务不存在",
            "205": "非法请求，不安全",
            "206": "接口版本过低，请联系本站(软件)开发者解决",
            "207": "配额不足，请联系本站(软件)开发者解决",
            "208": "验签失败，请联系本站(软件)开发者解决",
            "211": "请求参数超出上限",
            "212": "权限不足",
            "214": "接口版本过高，请联系本站(软件)开发者解决",
            "216": "内容违规",
            "224": "数据过大",
            '???': '严重未知错误，请联系本站(软件)开发者解决'
        }
        # 更多状态：https://quanmwl.yuque.com/docs/share/9fbd5429-6575-403d-8a3d-7081b2977eda?#8sz4 《平台状态码处理指引》

    def remove_empty(self, all_data):
        # type: (dict) -> dict
        """
        去除空值
        :param all_data: 目标数据
        :return:处理后数据
        """
        return_data = copy(all_data)
        for _a_data in all_data:
            if all_data[_a_data] is None:
                return_data.pop(_a_data)
        return return_data

    def encrypt(self, value):
        # type:(any) -> str
        """
        加密
        :param value: 目标数据
        :return:密文字符串
        """
        value = str(value).replace("'", '"')
        api_value = value
        if self.en == 'rsa':
            # 加载公钥
            print(self.PublicKeyStr)
            rsa_key = RSA.import_key(self.PublicKeyStr)

            # 加密
            cipher_rsa = Cipher_PKC.new(rsa_key)
            en_data = cipher_rsa.encrypt(str(value).encode("utf-8"))  # 加密
            # base64 进行编码
            base64_text = base64.b64encode(en_data)
            api_value = base64_text.decode()

        return api_value  # 返回字符串

    def send(self, direction, doc_id=None, data_object=None):
        # type:(str, str, str) -> tuple[bool, str, any, int]
        """
        发送请求(SDK内部调用，如有需要也可直接调用)
        :param direction: 数据方向（执行的操作类型）【必填】
        :param doc_id: 数据表名
        :param data_object: 数据内容（密文）
        :param need_id: 是否需要返回文档ID(默认不返回)
        :return: result set
        """
        headers = {
        }

        body = {
            'openID': self.open_id,
            'sign': '124141',
            'direction': direction,
            'en': self.en,
            'lcode': self.lcode_cipher,
            'docID': doc_id,
            'object': data_object
        }

        body = self.remove_empty(body)
        return_data = {}
        table_id = -1

        try:
            response = requests.post(f'{self.api_host}/v1/ndd', headers=headers, data=body)
            # http_status = response.status_code  几乎可以不依赖http状态码，如有需要请自行修改
        except:
            return False, 'Server Error', return_data, table_id
        _mess = 'Not Find'

        http_state = response.status_code
        # print(http_state)
        if http_state != 200 or response is None or 'HTML>' in response.text:
            if self.debug:
                print("Requests Fail")
            return False, _mess, return_data, table_id
        else:
            if self.debug:
                print(response.text)
            redata = json.loads(response.text)
            if 'state' not in redata:
                return False, _mess, return_data, table_id
            # print(redata)
            api_state = redata['state']
            if api_state in self.state_code:
                _mess = self.state_code[api_state]
            else:
                _mess = 'errCode:' + api_state
            if api_state == '200':
                if 'value' in redata or 'values' in redata:
                    return_data = redata['value'] if 'value' in redata else redata['values']
                if '__id' in redata:
                    table_id = redata['__id']
                return True, _mess, return_data, table_id
            else:
                return True, _mess, return_data, table_id

    def check_status(self):
        """
        状态检测
        :return:[描述/消息, 数据, tableID]
        """
        # 【状态检测】
        results, info, data, tid = self.send('STATUS')
        return info, data, tid

    def get_data(self, doc_id):
        """
        获取数据，如果为空返回标准格式空数据，但不会在数据库创建数据
        :param doc_id: 自定义文档id
        :return:[描述/消息, 数据, tableID]
        """
        # 【获取数据】如果为空返回标准格式空数据，但不会在数据库创建数据
        results, info, data, tid = self.send('GET', doc_id)
        return info, data, tid

    def set_data(self, doc_id, data):
        """
        保存数据，SET和GET操作时，如果成功，服务器都会返回最新的数据，请使用最新数据覆盖本地数据
        :param doc_id: 自定义文档id
        :param data: 数据的dict对象
        :return:[描述/消息, 数据, tableID]
        """
        # 【保存数据】
        # SET和GET操作时，如果成功，服务器都会返回最新的数据，请使用最新数据覆盖本地数据
        data_cip = self.encrypt(data)
        results, info, redata, tid = self.send('SET', doc_id, data_cip)
        return info, redata, tid

    def get_data_many(self, expression):
        """
        按条件获取多条数据
        :param expression: 条件表达式(key+运算符+值,如：number<=1024)
        :return:[描述/消息, 数据, tableID]
        """
        # 【按条件获取多条数据】
        results, info, datas, tid = self.send('getMany', expression)
        return info, datas, tid

    def del_data(self, doc_id):
        """
        按条件获取多条数据
        :param doc_id: 自定义文档id
        :return:[描述/消息, 数据, tableID]
        """
        # 【删除数据】
        results, info, datas, tid = self.send('DELETE', doc_id)
        return info, datas, tid

    def restore_data(self, table_id):
        """
        按条件获取多条数据
        :param table_id: 需要恢复的数据的tableID(也就是接口返回的__id)
        :return:[描述/消息, 数据, tableID]
        """
        # 【数据恢复，根据TableID恢复数据】
        results, info, datas, tid = self.send('restore', table_id)
        return info, datas, tid


if __name__ == '__main__':
    open_id = '2'  # 开发者ID  请修改替换：open_id\PublicKeyStr\lcode_cipher或lcode_plaintext
    # 不修改运行将会报错:ValueError: RSA key format is not supported

    # 接口和sdk目前支持两种模式使用:rsa加密模式以及明文模式(后者禁止用于传输敏感或涉及隐私和机密的数据)
    # =================================================================================
    # 【RSA加密模式】【开始】
    # lcode一般不会变动，在控制台证书列表中点击"加密"按钮获得,或在第三方工具将您的lcode使用公钥加密后粘贴进来
    lcode_cipher = "EJnzqJsDTMt1aSaDbhp3zuecv4bia/J1yuna+rTItMinJ3e43Hs0pGR5uyvBaPEL+YeP8gtthzPhAfkoOKpIbbof8VeY4MdZow3HXmgwfNld4xq42QiPAfKCjlJ40H48jxc5Zqg6hIO0p+KfjXp1GST547z2QaieuLEYUS449Kc="  # lcode密文
    PublicKeyStr = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqASIb3DQEBAQUAA4GNADCBiQKBgQC8Ql3WNz2XZyc6ESClqP5LigLk\nPVDUYSBIcremoTFsvBWg50C1uMJebRY1cNyK5o0hg7sdezJiL5D6kwY05x6Lx2Ms\nEX5DCOh/Kp0doSWecl5OdegxNn9M1EWzzjIEydWB9kew7kgYXhIrzhqRuQ1IpyNl\nEL91QP7MuPczwXtlAQIDAQAB\n-----END PUBLIC KEY-----"

    ndd_sdk = SDK(open_id, lcode_cipher, PublicKeyStr, True)  # 实例化SDK,生产环境建议关闭debug输出
    # 【RSA加密模式】【结束】
    # =======================================================================
    # 【明文不加密模式】【开始】 明文模式中，需要指定sdk最后一个参数(加密方式)的值为空
    # lcode_plaintext = "lcode-2222-43079-0000-aKGV-0000-6177-XX"  # lcode明文(实际使用不需要两个都写，这里是为了演示明文模式使用)
    #
    # ndd_sdk = SDK(open_id, lcode_plaintext, "", True, '')  # 实例化SDK,生产环境建议关闭debug输出
    # 【明文不加密模式】【结束】
    # =================================================================================

    # 这里演示了简单的操作
    random_data = random.randint(100000, 999999)  # 生成随机数据
    # 您的数据,以下演示一段常见的数据
    test_data = {
        "number":  289401788490,
        "dataTag": "apiTest",
        "url": "https://dev.quanmwl.com/console"
    }

    # info, data, tid = ndd_sdk.check_status()
    info, data, tid = ndd_sdk.get_data('PythonSDK_Test_003')  # 拉取数据，数据为空时返回标准的空数据
    # info, data, tid = ndd_sdk.set_data('PythonSDK_Test_003', test_data)  # 保存数据，将指定的数据存储到NDD
    # info, data, tid = ndd_sdk.get_data_many('number>1000')  # 拉取数据，数据为空时返回标准的空数据
    # info, data, tid = ndd_sdk.del_data('PythonSDK_Test_003')
    # info, data, tid = ndd_sdk.restore_data('16')
    print(info)
    print(data)
    print(tid)
