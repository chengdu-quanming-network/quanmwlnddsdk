# 通过NPM安装
NPM：
```shell
npm i quanmndd
```
gitee:[https://gitee.com/chengdu-quanming-network/quanmndd-nodejs](https://gitee.com/chengdu-quanming-network/quanmndd-nodejs)

# 使用示例
```JavaScripts
var SDK = require('./sdk')

// openID,在控制台可以看到，请替换为自己的
const open_id = '2'
// lcode，动态许可使用公钥加密后,请替换为自己控制台中的动态许可的加密密文
const lcode = 'EJnzqJsDTMt1aSaDbhp3zuecv4bia/J1yuna+rTItMinJ3e43Hs0pGR5uyvBaPEL+YeP8gtthzPhAfkoOKpIbbof8VeY4MdZow3HXmgwfNld4xq42QiPAfKCjlJ40H48jxc5Zqg6hIO0p+KfjXp1GST547z2QaieuLEYUS449Kc='
// publickey， 公钥,请替换为自己控制台下载的pem文件内容,换行用'\n'表示
const publickey = '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqASIb3DQEBAQUAA4GNADCBiQKBgQC8Ql3WNz2XZyc6ESClqP5LigLk\nPVDUYSBIcremoTFsvBWg50C1uMJebRY1cNyK5o0hg7sdezJiL5D6kwY05x6Lx2Ms\nEX5DCOh/Kp0doSWecl5OdegxNn9M1EWzzjIEydWB9kew7kgYXhIrzhqRuQ1IpyNl\nEL91QP7MuPczwXtlAQIDAQAB\n-----END PUBLIC KEY-----'

// 初始化SDK并获得实例  tip:正式环境中建议将debug设为false
const ndd_sdk = new SDK(open_id, lcode, publickey, true)
// 下面展示了部分示例代码，请按需解除注释以查看

// 接口及账户状态检查
ndd_sdk.checkStatus().then(({ success, info, data }) => {
  console.log('success:', success)
  console.log('info:', info)
  console.log('data', data)
}).catch(error => {
  console.log('Server Error')
  console.error(error)
})

// 获取数据
ndd_sdk.getData('NodeSDK_Test_003').then((redata) => {
  console.log('读取数据', redata)
  console.log('success:', redata.success)
  console.log('info:', redata.info)
  console.log('data', redata.data)
  console.log('TableID', redata.__id)
}).catch(err => {
  console.log('Server Error')
  console.error(err)
})
```