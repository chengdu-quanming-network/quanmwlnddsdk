# QuanmwlNDD_SDK

## 介绍

泉鸣开放平台文档数据库能力(旧名：ODSS)接口SDK(以及插件)集合目录。

- 如果您不了解Git或暂无gitee账号，可以下载最新发行版(nodejs可以使用npm i quanmndd)###暂无###。*该操作无需登录*
- 平台官网：[dev.quanmwl.com](https://dev.quanmwl.com)
- 开发者知识库(接口文档)
  ：[https://quanmwl.yuque.com/lx4ve0/vcsmy6/gelpzl7sw3pq30qx](https://quanmwl.yuque.com/lx4ve0/vcsmy6/gelpzl7sw3pq30qx)

## 兼容性和适配

- 请根据自己的实际需求，选择对应语言或系统的SDK/插件。
目前uTools版本的Demo开发已暂停，请勿下载使用

- 贡献者名单：
- [@泉鸣开放平台](http://dev.quanmwl.com)
- 适配清单(最新版本SDK)

| 语言/软件  | 状态检查 | 获取数据 | 更新数据 | 删除数据 | 恢复数据 | 明文模式 |
|--------|------|------|------|------|------|------|
| JS     | 施工中  | 施工中  | 施工中  | 施工中  | 施工中  | 施工中  | 
| PHP    | --   | --   | --   | --   | --   | --   |
| uTools | 施工中  | 施工中  | 施工中  | 施工中  | 施工中  | 施工中  | 
| NodeJS | √    | √    | √    | √    | √    |      | 
| Python | √    | √    | √    | √    | √    | √    | 
| Golang | --   | --   | --   | --   | --   |      | 

- (√：已支持且适配，--：暂未开发，×:不支持或待验证)

## 参与贡献

- 通过Gitee直接参与本仓库代码的编写以改进和维护SDK
- 提交轻量级PR(Pull Request Lite)或PR(Pull Request)
- 通过下载或clone代码到本地完成开发后，向官方客服提交代码及示例代码
- 申请加入仓库
- forks本仓库进行修改

## 贡献奖励

参考文档：

[《短信接口免费余额规则》](https://quanmwl.yuque.com/lx4ve0/vcsmy6/nvn0m2)
